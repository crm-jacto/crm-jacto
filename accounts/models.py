from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone


class UserProfile(models.Model):
    nome = models.CharField(max_length=100)
    email = models.EmailField()
    telefone = models.CharField(max_length=20)
    data_nascimento = models.DateField()
    cpf_cnpj = models.CharField(max_length=20)
    numero = models.CharField(max_length=10)
    cidade = models.CharField(max_length=100)
    cep = models.CharField(max_length=9)
    rua = models.CharField(max_length=255)
    
    def __str__(self):
        return self.nome

class Interaction(models.Model):
    OP_CHOICES = (
        ('Reunião', 'Reunião'),
        ('Email', 'Email'),
        ('Chamada', 'Chamada'),
        ('Chat', 'Chat'),
    )
    id_cliente = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    data = models.DateField(default=timezone.now)
    choice = models.CharField(max_length=10, choices=OP_CHOICES)
    descricao = models.CharField(max_length=200)

    def __str__(self):
        return f"Interacao {self.id} - Cliente: {self.id_cliente}, Tipo: {self.choice}, Data: {self.data}"

class Compra(models.Model):
    id_cliente = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    data = models.DateField(default=timezone.now)  # Define a data automaticamente
    valor = models.DecimalField(max_digits=10, decimal_places=2)
    produtos = models.CharField(max_length=200)

    def __str__(self):
        return f"Compra {self.id} - Cliente: {self.id_cliente}, Data: {self.data}"
    

class Preferencia(models.Model):
    id_cliente = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    preferencia = models.CharField(max_length=200)