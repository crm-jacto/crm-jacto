from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import *

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ['nome', 'email', 'telefone', 'data_nascimento', 'cpf_cnpj', 'numero', 'cidade', 'cep', 'rua']
        
class ClienteSearchForm(forms.Form):
    nome = forms.CharField(label='Nome', required=False)
    cpf_cnpj = forms.CharField(label='CPF/CNPJ', required=False)

class InteractionForm(forms.ModelForm):
    class Meta:
        model = Interaction
        fields = ['choice', 'descricao']

class CompraForm(forms.ModelForm):
    class Meta:
        model = Compra
        fields = ['valor', 'produtos']

class PreferenciaForm(forms.ModelForm):
    class Meta:
        model = Preferencia
        fields = ['preferencia']