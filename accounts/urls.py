from django.urls import path
from . import views

urlpatterns = [
    path('registro/', views.registrar, name='registro'),
    path('visualizar/', views.visualizar_clientes, name='visualizar_clientes'),
    path('pesquisar/', views.pesquisar_clientes, name='pesquisar_clientes'),
    path('editar_cliente/<int:pk>/', views.editar_cliente, name='editar_cliente'),
    path('adicionar_interacao/<int:cliente_id>/', views.adicionar_interacao, name='adicionar_interacao'),
    path('historico_interacoes/<int:cliente_id>/', views.historico_interacoes, name='historico_interacoes'),
    path('perfil/<int:cliente_id>/', views.visualizar_perfil, name='visualizar_perfil'),
    path('compras_e_preferencias/<int:cliente_id>/', views.lista_compras_e_preferencias, name='lista_compras_e_preferencias'),
    path('adicionar_compra/<int:cliente_id>/', views.adicionar_compra, name='adicionar_compra'),
    path('adicionar_preferencia/<int:cliente_id>/', views.adicionar_preferencia, name='adicionar_preferencia'),
]
