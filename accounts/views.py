from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.forms import UserCreationForm
from .forms import *
from .models import *

def registrar(request):
    if request.method == 'POST':
        form = UserProfileForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('visualizar_clientes')
    else:
        form = UserProfileForm()
    return render(request, 'accounts/registro.html', {'form': form})



def visualizar_clientes(request):
    # Recupera todos os perfis de usuário
    perfis = UserProfile.objects.all()
    return render(request, 'clientes/visualizar_clientes.html', {'perfis': perfis})

def visualizar_perfil(request, cliente_id):
    cliente = get_object_or_404(UserProfile, pk=cliente_id)
    return render(request, 'clientes/visualizar_perfil.html', {'cliente': cliente})

def pesquisar_clientes(request):
    if request.method == 'POST':
        form = ClienteSearchForm(request.POST)
        if form.is_valid():
            nome = form.cleaned_data['nome']
            cpf_cnpj = form.cleaned_data['cpf_cnpj']
            clientes = UserProfile.objects.filter(nome__icontains=nome, cpf_cnpj__icontains=cpf_cnpj)
            return render(request, 'clientes/resultado_pesquisa.html', {'clientes': clientes})
    else:
        form = ClienteSearchForm()
    return render(request, 'clientes/pesquisar_clientes.html', {'form': form})

def editar_cliente(request, pk):
    cliente = get_object_or_404(UserProfile, pk=pk)
    if request.method == 'POST':
        form = UserProfileForm(request.POST, instance=cliente)
        if form.is_valid():
            form.save()
            return redirect('visualizar_clientes')
    else:
        form = UserProfileForm(instance=cliente)
    return render(request, 'clientes/editar_cliente.html', {'form': form})

def historico_interacoes(request, cliente_id):
    cliente = UserProfile.objects.get(pk=cliente_id)
    interacoes = Interaction.objects.filter(id_cliente=cliente)
    return render(request, 'clientes/historico_interacoes.html', {'cliente': cliente, 'interacoes': interacoes})

def adicionar_interacao(request, cliente_id):
    cliente = UserProfile.objects.get(pk=cliente_id)
    if request.method == 'POST':
        form = InteractionForm(request.POST)
        if form.is_valid():
            interacao = form.save(commit=False)
            interacao.id_cliente = cliente
            interacao.save()
            return redirect('historico_interacoes', cliente_id=cliente.id)
    else:
        form = InteractionForm()
    return render(request, 'clientes/adicionar_interacao.html', {'form': form, 'cliente': cliente})

def lista_compras_e_preferencias(request, cliente_id):
    cliente = UserProfile.objects.get(pk=cliente_id)
    compras = cliente.compra_set.all()
    preferencias = cliente.preferencia_set.all()
    return render(request, 'compras/compras_preferencias.html', {'cliente': cliente, 'compras': compras, 'preferencias': preferencias})

def adicionar_compra(request, cliente_id):
    cliente = UserProfile.objects.get(pk=cliente_id)
    if request.method == 'POST':
        form = CompraForm(request.POST)
        if form.is_valid():
            compra = form.save(commit=False)
            compra.id_cliente = cliente
            compra.save()
            return redirect('lista_compras_e_preferencias', cliente_id=cliente.id)
    else:
        form = CompraForm()
    return render(request, 'compras/adicionar_compra.html', {'form': form})



def adicionar_preferencia(request, cliente_id):
    cliente = UserProfile.objects.get(pk=cliente_id)
    if request.method == 'POST':
        form = PreferenciaForm(request.POST)
        if form.is_valid():
            preferencia = form.save(commit=False)
            preferencia.id_cliente = cliente
            preferencia.save()
            return redirect('lista_compras_e_preferencias', cliente_id=cliente.id)
    else:
        form = PreferenciaForm()
    return render(request, 'compras/adicionar_preferencia.html', {'form': form})