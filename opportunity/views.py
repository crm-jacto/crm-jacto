from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.mail import send_mail
from django.template.loader import render_to_string
from .forms import *
from .models import *
import json


def opportunity_management(request):
    if request.method == 'POST':
        form = OpportunityForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('opportunity_management')
    else:
        form = OpportunityForm()
    
    opportunities = Opportunity.objects.all()
    prospects = opportunities.filter(etapa='prospeccao')
    negotiations = opportunities.filter(etapa='negociacao')
    closures = opportunities.filter(etapa='fechamento')
    wins = opportunities.filter(etapa='ganho')
    losses = opportunities.filter(etapa='perdido')
    
    return render(request, 'sales_cycle_tracking/opportunity_management.html', {
        'form': form,
        'prospects': prospects,
        'negotiations': negotiations,
        'closures': closures,
        'wins': wins,
        'losses': losses,
    })

@csrf_exempt
def update_opportunity_stage(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        opportunity_id = data.get('opportunity_id')
        new_stage = data.get('new_stage')
        
        try:
            opportunity = Opportunity.objects.get(pk=opportunity_id)
            opportunity.etapa = new_stage
            opportunity.save()
            return JsonResponse({'success': True})
        except Opportunity.DoesNotExist:
            return JsonResponse({'success': False, 'error': 'Oportunidade não encontrada'})
    
    return JsonResponse({'success': False, 'error': 'Método não permitido'})

def create_proposal(request):
    if request.method == 'POST':
        form = ProposalForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('listar_propostas')  # Corrigido para 'listar_propostas'
    else:
        form = ProposalForm()
    return render(request, 'sales_cycle_tracking/proposal.html', {'form': form})

def listar_propostas(request):
    propostas = Proposal.objects.all()
    return render(request, 'sales_cycle_tracking/list_proposal.html', {'propostas': propostas})

def detail_proposal(request, proposal_id):
    proposta = get_object_or_404(Proposal, pk=proposal_id)
    return render(request, 'sales_cycle_tracking/detail_proposal.html', {'proposta': proposta})

def excluir_proposta(request, proposal_id):
    proposta = get_object_or_404(Proposal, pk=proposal_id)
    if request.method == 'POST':
        proposta.delete()
        return redirect('listar_propostas')
    return render(request, 'sales_cycle_tracking/excluir_proposta.html', {'proposta': proposta})

from django.core.mail import send_mail
from django.template.loader import render_to_string

def enviar_proposta_por_email(request, proposal_id):
    proposta = get_object_or_404(Proposal, pk=proposal_id)
    # Lógica para obter os e-mails dos destinatários (suponha que estejam em um campo no modelo UserProfile)
    destinatarios = UserProfile.objects.values_list('email', flat=True)
    # Renderizar o conteúdo do e-mail a partir de um template
    email_content = render_to_string('sales_cycle_tracking/email_proposta.html', {'proposta': proposta})
    # Enviar e-mail para todos os destinatários
    send_mail(
        'Nova Proposta Disponível',
        email_content,
        '******@exemple.com',
        destinatarios,
        fail_silently=False,
    )
    return redirect('listar_propostas')