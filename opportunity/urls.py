from django.urls import path
from . import views

urlpatterns = [
    path('oportunidades/', views.opportunity_management, name='opportunity_management'),
    path('oportunidades/update_opportunity_stage/', views.update_opportunity_stage, name='update_opportunity_stage'),
    path('criar/', views.create_proposal, name='criar_proposta'),
    path('listar/', views.listar_propostas, name='listar_propostas'),
    path('detalhes/<int:proposal_id>/', views.detail_proposal, name='detalhes_proposta'),
    path('excluir/<int:proposal_id>/', views.excluir_proposta, name='excluir_proposta'),
    path('enviar_email/<int:proposal_id>/', views.enviar_proposta_por_email, name='enviar_proposta_por_email'),
]