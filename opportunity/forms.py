from django import forms
from .models import *

class OpportunityForm(forms.ModelForm):
    cliente = forms.ModelChoiceField(queryset=UserProfile.objects.all(), label='Cliente')  

    class Meta:
        model = Opportunity
        fields = ['cliente', 'etapa', 'valor']

class ProposalForm(forms.ModelForm):
    class Meta:
        model = Proposal
        fields = ['titulo', 'descricao', 'preco', 'prazo_validade', 'itens', 'termos_condicoes']
