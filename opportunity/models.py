from django.db import models
from django.contrib.auth.models import User
from accounts.models import UserProfile

class Opportunity(models.Model):
    STAGE_CHOICES = [
        ('prospeccao', 'Prospecção'),
        ('negociacao', 'Negociação'),
        ('fechamento', 'Fechamento'),
        ('ganho', 'Ganho'),
        ('perdido', 'Perdido'),
    ]

    cliente = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    etapa = models.CharField(max_length=20, choices=STAGE_CHOICES)
    valor = models.DecimalField(max_digits=10, decimal_places=2)
    data_criacao = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return f"Oportunidade para {self.cliente} - Etapa: {self.etapa}"

class Proposal(models.Model):
    titulo = models.CharField(max_length=100)
    descricao = models.CharField(max_length=100)
    preco = models.DecimalField(max_digits=10, decimal_places=2)
    prazo_validade = models.DateField()
    itens = models.CharField(max_length=100)
    termos_condicoes = models.CharField(max_length=100)

    def __str__(self):
        return self.titulo
