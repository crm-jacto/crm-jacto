function allowDrop(event) {
    event.preventDefault();
}

function drag(event) {
    event.dataTransfer.setData("text", event.target.id);
}

function drop(event) {
    event.preventDefault();
    var data = event.dataTransfer.getData("text");
    var destination = event.target.id;
    var opportunityId = data.split("_")[1];
    var newStage = destination;

    var csrfToken = document.getElementsByName('csrfmiddlewaretoken')[0].value;

    fetch('update_opportunity_stage/', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRFToken': csrfToken
        },
        body: JSON.stringify({
            'opportunity_id': opportunityId,
            'new_stage': newStage
        })
    })
    .then(response => {
        if (response.ok) {
            event.target.appendChild(document.getElementById(data));
            console.log("Oportunidade movida para a etapa: " + destination);
        } else {
            console.error('Falha ao atualizar o estágio da oportunidade');
        }
    })
    .catch(error => {
        console.error('Erro:', error);
    });
}
